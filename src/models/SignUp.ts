import { DateTime } from "ionic-angular";

export class SignUpModel{
  UserId : string;
  Password : any;
  FirstName : string;
  LastName : string;
  PictureUrl : string;
  DateTime : DateTime;
  Email : string;

}
