import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import 'rxjs/add/operator/toPromise';

/*
  Generated class for the ApibaseProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ApibaseProvider {
// private apiUrl: string = 'http://localhost/api';
get apiUrl(): string {
  return `http://${localStorage.getItem('apiUrl') || 'no-address'}/api`;
}

private headers = new HttpHeaders({
  'Content-Type': 'application/json'
});

constructor(private http: HttpClient) {}

async get<T>(path: string, params?: HttpParams): Promise<T> {
  return this.http
    .get<T>(`${this.apiUrl}/${path}`, {
      headers: this.headers,
      params: params
    })
    .toPromise();
}

async post<T>(path: string, body?: any): Promise<T> {
  return this.http
    .post<T>(`${this.apiUrl}/${path}`, body, {
      headers: this.headers
    })
    .toPromise();
}
}
