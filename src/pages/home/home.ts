import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Nav } from 'ionic-angular';

import { LoginPage } from '../login/login';

import { AngularFireAuth } from 'angularfire2/auth';
import { LoadingProvider } from '../../providers/loading/loading';
import { PageNavigation } from '../../models/NavPage';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  userData:any;
  rootPage: string;
  @ViewChild(Nav) nav: Nav;

  homePages: PageNavigation[];

  constructor(public afAuth: AngularFireAuth, public navCtrl: NavController, public navParam: NavParams,public loadingProvider : LoadingProvider) {

  		this.userData = this.navParam.get('res');
  		console.log('userData',this.userData);

      this.homePages = [
        { label: 'Tab Page', path: 'HometabsPage' }
      ];

      this.rootPage = this.homePages[0].path;
  }

  logout(){
    this.loadingProvider.startLoading();
  	this.afAuth.auth.signOut();
  	this.navCtrl.setRoot(LoginPage);
    this.loadingProvider.stopLoading();
  }

}
