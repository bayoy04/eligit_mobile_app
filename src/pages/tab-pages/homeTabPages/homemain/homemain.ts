import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { HomeProvider } from '../../../../providers/home/home';
import { Http } from '@angular/http';
import envData from '../../../../env-variables';
import { ChoicedetailsPage } from '../../../modals/choicedetails/choicedetails';

/**
 * Generated class for the HomemainPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-homemain',
  templateUrl: 'homemain.html',
})
export class HomemainPage {
  sample : string;
  pollOptions : string[];
  option : string[];
  finalOption : string[];
  show : boolean = false;
  isClick : boolean = true;
  keyFactor1 : string;
  keyFactor2: string;
  keyFactor3 : string;
  user : string;

  leadingFactor : string;

  listOfChoices : any;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public homeProvider : HomeProvider, 
    public http: Http,
    public loadingCtrl: LoadingController) {  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomemainPage');
    const account = JSON.parse(localStorage.getItem('accountInfo'));  
    this.user = account.FIRST_NAME;
    this.Choices();
  }

  Choices(){
    const loader = this.loadingCtrl.create({
	    	content: 'Please Wait...',
	  });
	  loader.present();
    let email = JSON.parse(localStorage.getItem('accountInfo'));

    this.http.get(envData.API_URL + '/listallchoices/recent/' + email.EMAIL_ADDRESS).map(res => res.json()).subscribe(
      response => {
        this.listOfChoices =response;
        loader.dismiss();
      });
  }

  getPollOptions(poll: string = ''): string[] {
   if(poll){
    return  poll.split(',') ;
   }else{
     return ['N/A'];
   }
  }

 ShowAnswer(index){
  this.show = index;
 }

 CloseAnswer(){
  this.show = false;
 }

 async choiceDeailsModal(i: any){
   localStorage.setItem('choiceDetails', JSON.stringify(this.listOfChoices[i]));
  this.navCtrl.setRoot('ChoicedetailsPage');
 }

 getSelectedKey1(){
   console.log(this.keyFactor1);
 }

 getSelectedKey2(){
  console.log(this.keyFactor2);
 }
 getSelectedKey3(){
  console.log(this.keyFactor3);
 }

 getFactor(){
  console.log(this.leadingFactor);
 }

}
