import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the HometabsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-hometabs',
  templateUrl: 'hometabs.html',
})
export class HometabsPage {
  Tab1:string = 'HomemainPage';
  Tab2:string = 'NetworkPage';
  Tab3:string = 'ShopPage';
  Tab4:string = 'NotificationPage';

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HometabsPage');
  }

}
