import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController, AlertController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { Http } from '@angular/http';
import { HomePage } from '../home/home';
import { RegisterPage } from '../register/register';
import { ForgetPage } from '../forget/forget';
import { AngularFireAuth } from 'angularfire2/auth';
import firebase from 'firebase';
import { LoadingProvider } from '../../providers/loading/loading';
import { Facebook } from '@ionic-native/facebook'

// NOTE
// --------------------------------------------------------------------------------------------------------------
// This file has been added to the gitignored files. You must create this file manually base on your preferences.
// create a file that is named "env-variables.ts" under the root folder named "src".
// Example env-variables.ts content:
// export default Object.assign({
// 	"API_URL": "http://c0f869d2.ngrok.io"
// });
import envData from '../../env-variables';
// --------------------------------------------------------------------------------------------------------------

@IonicPage()
@Component({
	selector: 'page-login',
	templateUrl: 'login.html',
	providers : []
})
export class LoginPage {

	userId : string = '';
	userPassword : string = '';
	userData:any;
	loginData = { email:'', password:'' };
	authForm : FormGroup;
	// userid: AbstractControl;
	// password: AbstractControl;
	passwordtype:string='password';
	passeye:string ='eye';

	constructor(
		public toastCtrl: ToastController, 
		public fb: FormBuilder, 
		public navCtrl: NavController, 
		public navParams: NavParams, 
		public afAuth: AngularFireAuth,
		public loadingProvider: LoadingProvider,
		public facebook: Facebook,
		public http: Http,
		public loadingCtrl: LoadingController,
		public alertCtrl: AlertController
	){
	// this.authForm = this.fb.group({
	//    'userId' : [null, Validators.compose([Validators.required])],
	//    'password': [null, Validators.compose([Validators.required])],
	//  });

	//  this.userid = this.authForm.controls['userId'];
	//  this.password = this.authForm.controls['password'];
	}

	ionViewDidLoad() {}

	login(){
		const loader = this.loadingCtrl.create({
	    	content: "Signing In. Please Wait...",
	    });
	    loader.present();

		let data = {
			"user_id": this.userId,
			"type":"postman",
			"email":"11test@gmail.com",
			"password": this.userPassword
		};

		for(var key in data){
			data[key] = data[key].trim();
		}

		console.log(data);

		// Note: This is now working. It returns the USER_ID, FIRST_NAME, LAST_NAME, EMAIL_ADDRESS, PASSWORD. 
		// But the CORS configuration is still not configured I think
		this.http.post(envData.API_URL + '/signin', data).subscribe(
			response => {
				localStorage.setItem('accountInfo', response['_body']);
				this.navCtrl.setRoot(HomePage);
				loader.dismiss();
			},
			error => {
				loader.dismiss();
				const alert = this.alertCtrl.create({
					title: 'Uh Oh!',
					subTitle: 'Given credentials is invalid. Please try again...',
					buttons: ['OK']
				});
				alert.present();
			}
		);
	}


	// For Social Login

	socialLogin(isLogin){
		if (isLogin == "facebook"){
			this.loadingProvider.startLoading();

			let provider = new firebase.auth.FacebookAuthProvider();
			firebase.auth().signInWithRedirect(provider).then(() => {
				this.loadingProvider.startLoading();
				firebase.auth().getRedirectResult().then((result) => {
					console.log('result',result);
					this.moveToHome(result.user);
					this.loadingProvider.stopLoading();
				}).catch(function(error){
					this.loadingProvider.stopLoading();
					alert(error.message);
					console.log('error',error);
				});
				this.loadingProvider.stopLoading();
			}).catch(function(error){
				this.loadingProvider.stopLoading();
				alert(error.message);
				console.log('error',error);
			})

			this.loadingProvider.stopLoading();
		} 
		else if(isLogin == "google"){
			this.loadingProvider.startLoading();
			let provider = new firebase.auth.GoogleAuthProvider();
			firebase.auth().signInWithRedirect(provider).then(() => {
				this.loadingProvider.startLoading();
				firebase.auth().getRedirectResult().then((result)=>{
					console.log('result',result);
					this.loadingProvider.stopLoading();
					this.moveToHome(result.user);
				}).catch(function(error){
					this.loadingProvider.stopLoading();
					alert(error.message);
					console.log('error',error);
				})
				this.loadingProvider.stopLoading();
			}).catch(function(error){
				this.loadingProvider.stopLoading();
				alert(error.message);
				console.log('error',error);
			});

			this.loadingProvider.stopLoading();
		}else if(isLogin == "twitter"){
			// this.afAuth.auth.signInWithPopup(new firebase.auth.TwitterAuthProvider())
			// 	.then(res => {
			// 		 this.moveToHome(res);
			// 	})
			// 	.catch(err => console.log('err',err));
		}else if(isLogin == "github"){
			// this.afAuth.auth.signInWithPopup(new firebase.auth.GithubAuthProvider())
			// 	.then(res => {
			// 		 this.moveToHome(res);
			// 	})
			// 	.catch(err => console.log('err',err));
		}

	}

	// Move to register page
	moveToRegister(){
		// this.navCtrl.setRoot(RegisterPage);
		this.navCtrl.push(RegisterPage);
	}

	//Move to Home Page
	moveToHome(res){
		console.log('res',res);
		this.navCtrl.setRoot(HomePage,{res:res});
	}

	presentToast(err) {
		const toast = this.toastCtrl.create({
			message: err.message,
			duration: 3000,
			position: 'bottom'
		});

		toast.present();
	}

	presentAlert(err) {

	}

	managePassword() {
		if(this.passwordtype == 'password'){
			this.passwordtype='text';
			this.passeye='eye-off';
		}
		else{
			this.passwordtype='password';
			this.passeye = 'eye';
		}
	}

	forgetpassword(){
		// this.navCtrl.setRoot(ForgetPage);
		this.navCtrl.push(ForgetPage);
	}

	policyStatement(){
		this.navCtrl.push('PolicyPage');
	}

	termsService(){
		this.navCtrl.push('TermsPage');
	}

	home(){
		this.navCtrl.setRoot(HomePage);
	}
}
