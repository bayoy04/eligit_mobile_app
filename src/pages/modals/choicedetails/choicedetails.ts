import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Chart } from 'chart.js';
import {PieColor} from '../../../models/pieColor'

/**
 * Generated class for the ChoicedetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-choicedetails',
  templateUrl: 'choicedetails.html',
})
export class ChoicedetailsPage {
  @ViewChild('pieChart') pieChart;
  pieChartEl : any;
  name :string;
  title : string;
  description : string;
   keyFactor1: string;
   keyFactor2: string;
   keyFactor3: string;
   totalVotes : string;
   colors : PieColor;
   chart : any;
   chartLabels : any    = [];
   chartValues : any =[];

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    const choiceDetails = JSON.parse(localStorage.getItem('choiceDetails'));
   
    this.name = choiceDetails.CREATED_NAME
    this.title = choiceDetails.TITLE;
    this.description = choiceDetails.DESCRIPTION;
    this.keyFactor1 = choiceDetails.KEY_FACTOR1;
    this.keyFactor2 = choiceDetails.KEY_FACTOR2;
    this.keyFactor3 = choiceDetails.KEY_FACTOR3;
    this.totalVotes = choiceDetails.TOTAL_VOTES;

    this.createPieChart();
  }

  createPieChart()
{

   this.pieChartEl 				= 	new Chart(this.pieChart.nativeElement,
   {
      type: 'pie',
      data: {
         labels: this.chartLabels =[this.keyFactor1, this.keyFactor2, this.keyFactor3],
         datasets: [{
            label                 : 'Daily Technology usage',
            data                  : this.chartValues =[this.totalVotes],
            duration              : 2000,
            easing                : 'easeInQuart',
            backgroundColor       : this.colors.color
         }]
      },
      options : {
         maintainAspectRatio: false,
         layout: {
            padding: {
               left     : 50,
               right    : 0,
               top      : 0,
               bottom   : 0
            }
         },
         animation: {
            duration : 5000
         }
      }
   });

   this.chart = this.pieChartEl.generateLegend();
}

}
