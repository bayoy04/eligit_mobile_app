import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChoicedetailsPage } from './choicedetails';

@NgModule({
  declarations: [
    ChoicedetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(ChoicedetailsPage),
  ],
})
export class ChoicedetailsPageModule {}
